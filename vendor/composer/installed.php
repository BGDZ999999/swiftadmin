<?php return array(
    'root' => array(
        'name' => 'meystack/swiftadmin',
        'pretty_version' => '2.0.0',
        'version' => '2.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'bacon/bacon-qr-code' => array(
            'pretty_version' => '2.0.7',
            'version' => '2.0.7.0',
            'reference' => 'd70c840f68657ce49094b8d91f9ee0cc07fbf66c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bacon/bacon-qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dasprid/enum' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => '5abf82f213618696dda8e3bf6f64dd042d8542b2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dasprid/enum',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '2.0.6',
            'version' => '2.0.6.0',
            'reference' => 'd9d313a36c872fd6ee06d9a6cbcf713eaa40f024',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'easywechat-composer/easywechat-composer' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'reference' => '3fc6a7ab6d3853c0f4e2922539b56cc37ef361cd',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../easywechat-composer/easywechat-composer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'endroid/qr-code' => array(
            'pretty_version' => '4.6.1',
            'version' => '4.6.1.0',
            'reference' => 'a75c913b0e4d6ad275e49a2c1de1cacffc6c2184',
            'type' => 'library',
            'install_path' => __DIR__ . '/../endroid/qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'a878d45c1914464426dc94da61c9e1d36ae262a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'gregwar/captcha' => array(
            'pretty_version' => 'v1.1.9',
            'version' => '1.1.9.0',
            'reference' => '4bb668e6b40e3205a020ca5ee4ca8cff8b8780c5',
            'type' => 'captcha',
            'install_path' => __DIR__ . '/../gregwar/captcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.5.0',
            'version' => '7.5.0.0',
            'reference' => 'b50a2a1251152e43f6a37f0fa053e730a67d25ba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.2',
            'version' => '1.5.2.0',
            'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.4.3',
            'version' => '2.4.3.0',
            'reference' => '67c26b443f348a51926030c83481b85718457d3d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'laravel/serializable-closure' => array(
            'pretty_version' => 'v1.2.2',
            'version' => '1.2.2.0',
            'reference' => '47afb7fae28ed29057fdca37e16a84f90cc62fae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/serializable-closure',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'meystack/swiftadmin' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '2.8.0',
            'version' => '2.8.0.0',
            'reference' => '720488632c590286b88b80e62aa3d3d551ad4a50',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nikic/fast-route' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/fast-route',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'opis/closure' => array(
            'pretty_version' => '3.6.3',
            'version' => '3.6.3.0',
            'reference' => '3d81e4309d2a927abbe66df935f4bb60082805ad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../opis/closure',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/pinyin' => array(
            'pretty_version' => '4.0.8',
            'version' => '4.0.8.0',
            'reference' => '04bdb4d33d50e8fb1aa5a824064c5151c4b15dc2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/pinyin',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/socialite' => array(
            'pretty_version' => '4.6.3',
            'version' => '4.6.3.0',
            'reference' => '52033511df3795c568528fe70fbe41459135911b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/socialite',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/wechat' => array(
            'pretty_version' => '5.31.0',
            'version' => '5.31.0.0',
            'reference' => 'cfd0feb6e0bee504d2f39d141e2b382d25fc441b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/wechat',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-di/invoker' => array(
            'pretty_version' => '2.3.3',
            'version' => '2.3.3.0',
            'reference' => 'cd6d9f267d1a3474bdddf1be1da079f01b942786',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-di/invoker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-di/php-di' => array(
            'pretty_version' => '6.4.0',
            'version' => '6.4.0.0',
            'reference' => 'ae0f1b3b03d8b29dff81747063cbfd6276246cc4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-di/php-di',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-di/phpdoc-reader' => array(
            'pretty_version' => '2.2.1',
            'version' => '2.2.1.0',
            'reference' => '66daff34cbd2627740ffec9469ffbac9f8c8185c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-di/phpdoc-reader',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.6.5',
            'version' => '6.6.5.0',
            'reference' => '8b6386d7417526d1ea4da9edb70b8352f7543627',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => '1.9.0',
            'version' => '1.9.0.0',
            'reference' => 'dc5ff11e274a90cc1c743f66c9ad700ce50db9ab',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'pimple/pimple' => array(
            'pretty_version' => 'v3.5.0',
            'version' => '3.5.0.0',
            'reference' => 'a94b3a4db7fb774b3d78dad2315ddc07629e1bed',
            'type' => 'library',
            'install_path' => __DIR__ . '/../pimple/pimple',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '^1.0',
            ),
        ),
        'psr/event-dispatcher' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'ef29f6d262798707a9edd554e2b82517ef3a9376',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0 || 2.0.0 || 3.0.0',
                1 => '1.0|2.0|3.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache' => array(
            'pretty_version' => 'v5.4.15',
            'version' => '5.4.15.0',
            'reference' => '60e87188abbacd29ccde44d69c5392a33e888e98',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '64be4a7acb83b6f2bf6de9a02cee6dad41277ebc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/console' => array(
            'pretty_version' => 'v6.0.15',
            'version' => '6.0.15.0',
            'reference' => 'b0b910724a0a0326b4481e4f8a30abb2dd442efb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v6.0.9',
            'version' => '6.0.9.0',
            'reference' => '5c85b58422865d42c6eb46f7693339056db098a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => '7bc61cc2db649b4637d331240c5346dcc7708051',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.0|3.0',
            ),
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v6.0.11',
            'version' => '6.0.11.0',
            'reference' => '09cb683ba5720385ea6966e5e06be2a34f2568b1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v6.0.15',
            'version' => '6.0.15.0',
            'reference' => 'a93829f4043fdcddebabd8433bdb46c2dcaefe06',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '511a08c03c1960e08a883f4cffcacd219b758354',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/psr-http-message-bridge' => array(
            'pretty_version' => 'v2.1.3',
            'version' => '2.1.3.0',
            'reference' => 'd444f85dddf65c7e57c58d8e5b3a4dbb593b1840',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/psr-http-message-bridge',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v6.0.15',
            'version' => '6.0.15.0',
            'reference' => '51ac0fa0ccf132a00519b87c97e8f775fa14e771',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v5.4.14',
            'version' => '5.4.14.0',
            'reference' => 'f0ed07675863aa6e3939df8b1bc879450b585cab',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '136b19dd05cdf0709db6537d058bcab6dd6e2dbe',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3',
            ),
        ),
        'symfony/var-exporter' => array(
            'pretty_version' => 'v6.0.10',
            'version' => '6.0.10.0',
            'reference' => 'e3df004a8d0fb572c420a6915cd23db9254c8366',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-exporter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-cache' => array(
            'pretty_version' => 'v2.0.6',
            'version' => '2.0.6.0',
            'reference' => '75a56b24affc65b51688fd89ada48c102757fd74',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-container' => array(
            'pretty_version' => 'v2.0.5',
            'version' => '2.0.5.0',
            'reference' => '2189b39e42af2c14203ed4372b92e38989e9dabb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-image' => array(
            'pretty_version' => 'v1.0.7',
            'version' => '1.0.7.0',
            'reference' => '8586cf47f117481c6d415b20f7dedf62e79d5512',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-image',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.55',
            'version' => '2.0.55.0',
            'reference' => 'e1974a4c3b1b4c5b808fcc0863fc254e711dee13',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-template' => array(
            'pretty_version' => 'v2.0.8',
            'version' => '2.0.8.0',
            'reference' => 'abfc293f74f9ef5127b5c416310a01fe42e59368',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-validate' => array(
            'pretty_version' => 'v2.0.2',
            'version' => '2.0.2.0',
            'reference' => '857f9bffc1a09a41e3969a19726cb04315848f0f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-validate',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v5.5.0',
            'version' => '5.5.0.0',
            'reference' => '1a7ea2afc49c3ee6d87061f5a233e3a035d0eae7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/console' => array(
            'pretty_version' => 'v1.2.12',
            'version' => '1.2.12.0',
            'reference' => '3f86741e4c2d9e8a48cd419721c75a8fd74d2a37',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/console',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/event' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'db17d2fd6a5a8799f97f587b17a26b814901e01a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/event',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/gateway-worker' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'reference' => '4921663553b4f9f15bb4bf5207f6a324b0926e24',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/gateway-worker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/redis-queue' => array(
            'pretty_version' => 'v1.2.4',
            'version' => '1.2.4.0',
            'reference' => '81667bf9ab3c1256e2c2f61b9d41d53791b8b34b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/redis-queue',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/think-cache' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'reference' => '25bd103d7fc9347aca680e677282db761cc90a43',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/think-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/think-orm' => array(
            'pretty_version' => 'v1.0.11',
            'version' => '1.0.11.0',
            'reference' => '174876df10d917b81a5bbffef56dbc9628e9bd9a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/gateway-worker' => array(
            'pretty_version' => 'v3.0.25',
            'version' => '3.0.25.0',
            'reference' => '5b47eb9a90c6b2afc25327979e41de352cb3c286',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/gateway-worker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/redis' => array(
            'pretty_version' => 'v1.0.11',
            'version' => '1.0.11.0',
            'reference' => '14f77108d3498fbc84cd8b10cb48c18f09661458',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/redis',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/redis-queue' => array(
            'pretty_version' => 'v1.0.10',
            'version' => '1.0.10.0',
            'reference' => 'b8286b4086a852fd588a98453f3dc7ed63bd79fe',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/redis-queue',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/webman-framework' => array(
            'pretty_version' => 'v1.4.7',
            'version' => '1.4.7.0',
            'reference' => 'e9815557f08dffd3a41b54f709a98619aab84f16',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/webman-framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/workerman' => array(
            'pretty_version' => 'v4.1.4',
            'version' => '4.1.4.0',
            'reference' => '83e007acf936e2233ac92d7368b87716f2bae338',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/workerman',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'yansongda/pay' => array(
            'pretty_version' => 'v3.1.12',
            'version' => '3.1.12.0',
            'reference' => '7ff004f05f9d6e288ff9b4deef585d30395f37f2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../yansongda/pay',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'yansongda/supports' => array(
            'pretty_version' => 'v3.2.5',
            'version' => '3.2.5.0',
            'reference' => 'c3f736efe169696cef94730976e604a61c345b5c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../yansongda/supports',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
